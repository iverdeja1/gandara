<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auxiliares".
 *
 * @property int $id
 * @property string|null $nombreAuxiliar
 *
 * @property Asuntos[] $asuntos
 * @property Atenciones[] $atenciones
 * @property Horas[] $horas
 * @property Pacientes[] $idPacientes
 * @property Telefonos[] $telefonos
 */
class Auxiliares extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auxiliares';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreAuxiliar'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'nombreAuxiliar' => 'Nombre Auxiliar',
        ];
    }

    /**
     * Gets query for [[Asuntos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsuntos()
    {
        return $this->hasMany(Asuntos::class, ['idAuxiliares' => 'id']);
    }

    /**
     * Gets query for [[Atenciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAtenciones()
    {
        return $this->hasMany(Atenciones::class, ['idAuxiliares' => 'id']);
    }

    /**
     * Gets query for [[Horas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHoras()
    {
        return $this->hasMany(Horas::class, ['idAuxiliares' => 'id']);
    }

    /**
     * Gets query for [[IdPacientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes()
    {
        return $this->hasMany(Pacientes::class, ['id' => 'idPacientes'])->viaTable('atenciones', ['idAuxiliares' => 'id']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::class, ['idAuxiliares' => 'id']);
    }
}
