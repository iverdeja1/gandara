<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asuntos".
 *
 * @property int $id
 * @property string|null $fecha
 * @property int|null $cantidadAsuntos
 * @property int|null $idAuxiliares
 *
 * @property Auxiliares $idAuxiliares0
 */
class Asuntos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asuntos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['cantidadAsuntos', 'idAuxiliares'], 'integer'],
            [['idAuxiliares'], 'exist', 'skipOnError' => true, 'targetClass' => Auxiliares::class, 'targetAttribute' => ['idAuxiliares' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'idAuxiliares' => 'Id Auxiliares',
            'fecha' => 'Fecha',
            'cantidadAsuntos' => 'Cantidad Asuntos',
           
        ];
    }

    /**
     * Gets query for [[IdAuxiliares0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuxiliares0()
    {
        return $this->hasOne(Auxiliares::class, ['id' => 'idAuxiliares']);
    }
}
