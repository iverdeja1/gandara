<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "horas".
 *
 * @property int $id
 * @property string|null $fecha
 * @property int|null $cantidadHoras
 * @property int|null $idAuxiliares
 *
 * @property Auxiliares $idAuxiliares0
 */
class Horas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'horas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['cantidadHoras', 'idAuxiliares'], 'integer'],
            [['idAuxiliares'], 'exist', 'skipOnError' => true, 'targetClass' => Auxiliares::class, 'targetAttribute' => ['idAuxiliares' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'idAuxiliares' => 'Id Auxiliares',
            'fecha' => 'Fecha',
            'cantidadHoras' => 'Cantidad Horas',
            
        ];
    }

    /**
     * Gets query for [[IdAuxiliares0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuxiliares0()
    {
        return $this->hasOne(Auxiliares::class, ['id' => 'idAuxiliares']);
    }
}
