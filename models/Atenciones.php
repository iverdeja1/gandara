<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "atenciones".
 *
 * @property int $id
 * @property int|null $idPacientes
 * @property int|null $idAuxiliares
 *
 * @property Auxiliares $idAuxiliares0
 * @property Pacientes $idPacientes0
 */
class Atenciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atenciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPacientes', 'idAuxiliares'], 'integer'],
            [['idPacientes', 'idAuxiliares'], 'unique', 'targetAttribute' => ['idPacientes', 'idAuxiliares']],
            [['idAuxiliares'], 'exist', 'skipOnError' => true, 'targetClass' => Auxiliares::class, 'targetAttribute' => ['idAuxiliares' => 'id']],
            [['idPacientes'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPacientes' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'idPacientes' => 'Id Pacientes',
            'idAuxiliares' => 'Id Auxiliares',
        ];
    }

    /**
     * Gets query for [[IdAuxiliares0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuxiliares0()
    {
        return $this->hasOne(Auxiliares::class, ['id' => 'idAuxiliares']);
    }

    /**
     * Gets query for [[IdPacientes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPacientes']);
    }
}
