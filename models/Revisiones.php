<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "revisiones".
 *
 * @property int $id
 * @property int|null $manos
 * @property int|null $pies
 * @property int|null $idPacientes
 * @property string|null $fechaManos
 * @property string|null $fechaPies
 * @property string|null $observacion
 *
 * @property Pacientes $idPacientes0
 */
class Revisiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'revisiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manos', 'pies', 'idPacientes'], 'integer'],
            [['fechaManos', 'fechaPies'], 'safe'],
            [['observacion'], 'string', 'max' => 500],
            [['idPacientes'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPacientes' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'idPacientes' => 'Id Pacientes',
            'fechaManos' => 'Fecha Manos',
            'manos' => 'Manos',
            'fechaPies' => 'Fecha Pies',
            'pies' => 'Pies',                               
            'observacion' => 'Observacion',
        ];
    }

    /**
     * Gets query for [[IdPacientes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPacientes']);
    }
}
