<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menstruaciones".
 *
 * @property int $id
 * @property int|null $positivo
 * @property string|null $fecha
 * @property int|null $idPacientes
 *
 * @property Pacientes $idPacientes0
 */
class Menstruaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menstruaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['positivo', 'idPacientes'], 'integer'],
            [['fecha'], 'safe'],
            [['idPacientes'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPacientes' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'idPacientes' => 'Id Pacientes',
            'fecha' => 'Fecha',
            'positivo' => 'Positivo',
            
            
        ];
    }

    /**
     * Gets query for [[IdPacientes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPacientes']);
    }
}
