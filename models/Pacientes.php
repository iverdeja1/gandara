<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pacientes".
 *
 * @property int $id
 * @property int|null $numeroHistoria
 * @property string|null $fechaNacimiento
 * @property string|null $nombreCompleto
 *
 * @property Atenciones[] $atenciones
 * @property Camas $camas
 * @property Deposiciones[] $deposiciones
 * @property Auxiliares[] $idAuxiliares
 * @property Menstruaciones[] $menstruaciones
 * @property Revisiones[] $revisiones
 */
class Pacientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pacientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numeroHistoria'], 'integer'],
            [['fechaNacimiento'], 'safe'],
            [['nombreCompleto'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
             'nombreCompleto' => 'Nombre Completo',
            'numeroHistoria' => 'Numero Historia',
            'fechaNacimiento' => 'Fecha Nacimiento',
           
        ];
    }

    /**
     * Gets query for [[Atenciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAtenciones()
    {
        return $this->hasMany(Atenciones::class, ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[Camas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamas()
    {
        return $this->hasOne(Camas::class, ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[Deposiciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeposiciones()
    {
        return $this->hasMany(Deposiciones::class, ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[IdAuxiliares]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuxiliares()
    {
        return $this->hasMany(Auxiliares::class, ['id' => 'idAuxiliares'])->viaTable('atenciones', ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[Menstruaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenstruaciones()
    {
        return $this->hasMany(Menstruaciones::class, ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[Revisiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRevisiones()
    {
        return $this->hasMany(Revisiones::class, ['idPacientes' => 'id']);
    }
}
