<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deposiciones".
 *
 * @property int $id
 * @property string|null $turno
 * @property string|null $fecha
 * @property int|null $positivo
 * @property int|null $idPacientes
 *
 * @property Pacientes $idPacientes0
 */
class Deposiciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deposiciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['positivo', 'idPacientes'], 'integer'],
            [['turno'], 'string', 'max' => 500],
            [['idPacientes'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPacientes' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'idPacientes' => 'Id Pacientes',
             'fecha' => 'Fecha',
            'turno' => 'Turno',
            'positivo' => 'Positivo',
            
        ];
    }

    /**
     * Gets query for [[IdPacientes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPacientes']);
    }
}
