<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Pacientes;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
        public function actionMostrarpacientes(){
        $dataProvider = new ActiveDataProvider([
        'query' => Pacientes::find(),
        'pagination' => [
            'pageSize' => 10, // Ajusta según sea necesario
        ],
    ]);

    return $this->render('pacientes2', [
        'dataProvider' => $dataProvider,
    ]);
    }
    
    public function actionRegistros()
    {
    return $this->render('registros'); // Este es el nombre de tu vista en el archivo 'registros.php'
    }
    
    
    public function actionVerprotocolos() {
    // Ruta al archivo PDF
    $pdfPath = Yii::getAlias('@webroot') . '/documentos/rcp.pdf';

    // Verificar si el archivo existe
    if (file_exists($pdfPath)) {
        // Devolver el contenido del PDF al navegador
        return Yii::$app->response->sendFile($pdfPath, 'rcp.pdf', ['inline' => true]);
    } else {
        // Manejar el caso en que el archivo no existe
        throw new \yii\web\NotFoundHttpException('El archivo PDF no se encuentra.');
    }
}

  public function actionProtocolos()
    {
    return $this->render('protocolos'); // Este es el nombre de tu vista en el archivo 'registros.php'
    }
    
}
