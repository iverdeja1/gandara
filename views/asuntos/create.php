<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Asuntos $model */

$this->title = 'Create Asuntos';
$this->params['breadcrumbs'][] = ['label' => 'Asuntos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asuntos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
