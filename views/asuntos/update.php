<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Asuntos $model */

$this->title = 'Update Asuntos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asuntos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asuntos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
