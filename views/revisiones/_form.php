<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Revisiones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="revisiones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'manos')->textInput() ?>

    <?= $form->field($model, 'pies')->textInput() ?>

    <?= $form->field($model, 'idPacientes')->textInput() ?>

    <?= $form->field($model, 'fechaManos')->textInput() ?>

    <?= $form->field($model, 'fechaPies')->textInput() ?>

    <?= $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
