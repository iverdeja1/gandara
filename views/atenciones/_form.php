<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Atenciones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="atenciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPacientes')->textInput() ?>

    <?= $form->field($model, 'idAuxiliares')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
