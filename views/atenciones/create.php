<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Atenciones $model */

$this->title = 'Create Atenciones';
$this->params['breadcrumbs'][] = ['label' => 'Atenciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atenciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
