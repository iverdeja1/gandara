<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\BootstrapAsset;
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">      
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">GANDARA</h1>

        <p class="lead">Cuidado de los pacientes.</p>

        <div class="btn-group" role="group">
        <a class="btn btn-lg btn-success mr-3" href="<?= Yii::$app->urlManager->createUrl(['/site/registros']) ?>">Registros</a>
        <a class="btn btn-lg btn-success" href="<?= Yii::$app->urlManager->createUrl(['/site/protocolos']) ?>">Protocolos</a>

    </div>
    </div>

       
<div class="body-content">
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="card alturaminima tarjeta-separada">
                <div class="card-body tarjeta">
                    <h3>Menstruaciones</h3>
                    <p>Acceso al registro de mentruaciones de los pacientes de la unidad.</p>
                    <p>
                        <?= Html::a('Menstruaciones', ['menstruaciones/index'], ['class' => 'btn btn-primary'])?>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-4">
            <div class="card alturaminima tarjeta-separada">
                <div class="card-body tarjeta">
                    <h3>Revisiones</h3>
                    <p>Acceso al registro de revisiones de los pacientes de la unidad.</p>
                    <p>
                        <?= Html::a('Revisiones', ['revisiones/index'], ['class' => 'btn btn-primary'])?>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-4">
            <div class="card alturaminima tarjeta-separada">
                <div class="card-body tarjeta">
                    <h3>Deposiciones</h3>
                    <p>Acceso al registro de deposiciones de los pacientes de la unidad.</p>
                    <p>
                        <?= Html::a('Deposiciones', ['deposiciones/index'], ['class' => 'btn btn-primary'])?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


    
    
    
        </div>

            <div class="card-deck text-center mt-2">
            <div class="card mx-2">
                <a href="<?= Url::to('site/mostrarpacientes') ?>" class="card-body btn btn" data-title="Censo">
                    <img src="<?= Yii::getAlias('@web') ?>/images/censo.png" alt="Censo">
                </a>
            </div>
            <div class="card mx-2">
                <a href="<?= Url::to(['site/mostrarpacientes']) ?>" class="card-body btn btn-sm" data-title="Mapa de camas">
                    <img src="<?= Yii::getAlias('@web') ?>/images/mapa.png" alt="Censo">
                </a>
            </div>
            <div class="card mx-2">
                <a href="<?= Url::to(['site/mostrarpacientes']) ?>" class="card-body btn btn-sm" data-title="Alertas">
                    <img src="<?= Yii::getAlias('@web') ?>/images/alerta.png" alt="Censo">
                </a>
            </div>
        </div>


