
<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\BootstrapAsset;


// Definición del título
$this->title = 'Censo de los pacientes';

?>


<!--Este el el código del widget del ListView-->
<div class="mt-5"> <!-- Agregamos un margen superior de 3 unidades de espacio -->
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_pacientes2',
        'layout' => "{items}\n{pager}",
        'options' => ['class' => 'list-view'], // Clase de Bootstrap
        'itemOptions' => ['class' => 'media'], // Clase de Bootstrap para cada elemento
    ]); ?>
</div>