<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

// $model representa un modelo de ciclista que será proporcionado por el ListView
?>


<!--Este es el código que determina cómo se muestran los datos y qué maquetación tienen en el ListView-->
<div class="media">
    <div class="media-body">
        <h4 class="media-heading"><?='Nombre completo: ' . Html::encode($model->nombreCompleto) ?></h4>
        <p><?= 'Id: ' . Html::encode($model->id) ?></p>
        <p><?= 'Número de historia: ' . Html::encode($model->numeroHistoria) ?></p>
        <p><?= 'Fecha de nacimiento: ' . Html::encode($model->fechaNacimiento) ?></p>
    </div>
</div>
