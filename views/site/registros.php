<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Registros';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .tarjeta {
        margin: 10px; /* Margen entre las tarjetas */
    }
</style>

<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Asuntos</h3>
                <p>Registro de asuntos.</p>
                <p>
                    <?= Html::a('Asuntos', ['/asuntos/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Atenciones</h3>
                <p>Registro de atenciones.</p>
                <p>
                    <?= Html::a('Atenciones', ['/atenciones/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Auxiliares</h3>
                <p>Registro de auxiliares.</p>
                <p>
                    <?= Html::a('Auxiliares', ['/auxiliares/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Camas</h3>
                <p>Registro de camas.</p>
                <p>
                    <?= Html::a('Camas', ['/camas/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Deposiciones</h3>
                <p>Registro de deposiciones.</p>
                <p>
                    <?= Html::a('Deposiciones', ['/deposiciones/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Habitaciones</h3>
                <p>Registro de habitaciones.</p>
                <p>
                    <?= Html::a('Habitaciones', ['/habitaciones/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Horas</h3>
                <p>Registro de horas.</p>
                <p>
                    <?= Html::a('Horas', ['/horas/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Menstruaciones</h3>
                <p>Registro de menstruaciones.</p>
                <p>
                    <?= Html::a('Menstruaciones', ['/menstruaciones/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Pacientes</h3>
                <p>Registro de pacientes.</p>
                <p>
                    <?= Html::a('Pacientes', ['/pacientes/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Revisiones</h3>
                <p>Registro de revisiones.</p>
                <p>
                    <?= Html::a('Revisiones', ['/revisiones/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Deposiciones</h3>
                <p>Registro de deposiciones.</p>
                <p>
                    <?= Html::a('Deposiciones', ['/deposiciones/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6 col-md-4">
        <div class="card alturaminima tarjeta-separada">
            <div class="card-body tarjeta">
                <h3>Teléfonos</h3>
                <p>Registro de Teléfonos.</p>
                <p>
                    <?= Html::a('Teléfonos', ['/telefonos/index'], ['class' => 'btn btn-primary']) ?>
                </p>
            </div>
        </div>
    </div> 
    <!-- Repite el mismo patrón para los otros modelos -->
</div> 