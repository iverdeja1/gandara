<?php

use yii\helpers\Html;

$this->title = 'Protocolos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-protocolos">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo Html::a('Protocolo de RCP', ['/site/verprotocolos'], ['class' => 'btn btn-primary', 'target' => '_blank']);
            ?>
        </div>
    </div>
</div>



