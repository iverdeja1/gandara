<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Deposiciones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="deposiciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'turno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'positivo')->textInput() ?>

    <?= $form->field($model, 'idPacientes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
