<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Deposiciones $model */

$this->title = 'Update Deposiciones: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Deposiciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="deposiciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
