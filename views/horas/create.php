<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Horas $model */

$this->title = 'Create Horas';
$this->params['breadcrumbs'][] = ['label' => 'Horas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
