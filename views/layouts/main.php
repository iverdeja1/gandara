<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <!<!-- Metadatos -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="author" content="Irene Verdeja Díaz">
    <meta name="description" content="Aplicación para el cuidado de pacientes">
    <meta name="keywords" content="HTML, CSS, JavaScript, React">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    
    <!<!-- Título -->
    <title>Gandara | Aplicación web para el cuidado de pacientes</title>
    <?php $this->head() ?>
    
    <!<!-- Favicon -->
    <link rel="shortcut icon" href="gandaraFavicon.ico" />
    <link rel="icon" type="image/x-icon" href="gandaraFavicon.ico"/>
</head>
<body class="d-flex flex-column h-100 body" >
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/LogoGandara.png', ['alt'=>Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        
        /*Barra de navegacón*/
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            
            ['label' => 'Asuntos', 'url' => ['/asuntos/index']],
            ['label' => 'Atenciones', 'url' => ['/atenciones/index']],
            ['label' => 'Auxiliares', 'url' => ['/auxiliares/index']],
            ['label' => 'Camas', 'url' => ['/camas/index']],
            ['label' => 'Deposiciones', 'url' => ['/deposiciones/index']],
            ['label' => 'Habitaciones', 'url' => ['/habitaciones/index']],
            ['label' => 'Horas', 'url' => ['/horas/index']],
            ['label' => 'Menstruaciones', 'url' => ['/menstruaciones/index']],
            ['label' => 'Pacientes', 'url' => ['/pacientes/index']],
            ['label' => 'Revisiones', 'url' => ['/revisiones/index']],
            ['label' => 'Telefonos', 'url' => ['/telefonos/index']],
            
//            ['label' => 'About', 'url' => ['/site/about']],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; My Company <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
