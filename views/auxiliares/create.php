<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Auxiliares $model */

$this->title = 'Create Auxiliares';
$this->params['breadcrumbs'][] = ['label' => 'Auxiliares', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auxiliares-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
